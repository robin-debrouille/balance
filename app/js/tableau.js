function poubelle(){
    $('.poubelle').click(function(){
       $(this).parent('tr').remove();
       $.ajax({
            url:'http://localhost:8000/suppr_tableau.php',
            method: 'POST',
            data:{
                id:  $(this).parent('tr').attr('id').substr(3),
            },
        });
    });
}

function switchES(){  
    $('.switch').click(function(){
            if($(this).parents('.colone').hasClass('sortie')){
                var es='sortie';
            }
            if($(this).parents('.colone').hasClass('entre')){
                var es='entre';
            }
            if(es === 'entre'){
                $('.sortie').find('table').append($(this).parent('tr'));
            }
            if(es === 'sortie'){
                $('.entre').find('table').append($(this).parent('tr'));
            }
            $.ajax({
            url:'http://localhost:8000/switch_tableau.php',
            method: 'POST',
            data:{
                id:  $(this).parent('tr').attr('id').substr(3),
                es:  es
            },
        });
    });
}

function SuccessAjout(result){
    return result; 
}

function SuccessLoad(result){
    var lignes = JSON.parse(result);
    lignes.forEach(function(ligne){
        if (ligne.es === "entre" && ligne.supp === "0"){
            // $('#entre table tbody').append('\
            //   <tr id="id-'+ligne.id+'">\n\
            //     <td>'+ligne.nom+'</td>\n\
            //     <td>'+ligne.poid+'</td>\n\
            //     <td class="switch"><img  src="img/exchange.png"/></td>\n\
            //     <td class="poubelle"><img src="img/trash.png"/></td>\n\
            //   </tr>');
            $('#entre table tbody').append('\
              <tr id="id-'+ligne.id+'">\n\
                <td>'+ligne.nom+'</td>\n\
                <td>'+ligne.poid+'</td>\n\
              </tr>');
        }
        if (ligne.es === "sortie" && ligne.supp === "0"){
            // $('#sortie table tbody').append('\
            //   <tr id="id-'+ligne.id+'">\n\
            //     <td>'+ligne.nom+'</td>\n\
            //     <td>'+ligne.poid+'</td>\n\
            //     <td class="switch"><img  src="img/exchange.png"/></td>\n\
            //     <td class="poubelle"><img src="img/trash.png"/></td>\n\
            //   </tr>');
            $('#sortie table tbody').append('\
              <tr id="id-'+ligne.id+'">\n\
                <td>'+ligne.nom+'</td>\n\
                <td>'+ligne.poid+'</td>\n\
              </tr>');
        }
    });
    $('.poubelle').unbind('click');
    poubelle();
    $('.switch').unbind('click');
    switchES();
}


//connection formulaire tableau
$( document ).ready(function() {
    $.ajax({
        url:'http://localhost:8000/chargement_tableau.php',
        method: 'POST',
        success: SuccessLoad
    });
    $('.form .ajouter').click(function() {
        if ($(this).parent('.form').find('input.label__poid').val() !== "" &&
            $(this).parent('.form').find('input.label__type').val() !== "" && 
            $.isNumeric($(this).parent('.form').find('input.label__poid').val())){
            var id=$.ajax({
                    url:'http://localhost:8000/entre_tableau.php',
                    method: 'POST',
                    data:{
                        nom: $(this).parents('.colone').find('input.label__type').val(),
                        poid: $(this).parents('.colone').find('input.label__poid').val(),
                        colonne: $(this).parents('.colone').attr("id")
                        
                    },
                    success: SuccessAjout
            });
            // $(this).parent('.form').parent('.colone').find('table tbody').append('\
            //   <tr id="id-'+id+'">\n\
            //     <td>'+$(this).parents('.colone').find('input.label__type').val()+'</td>\n\
            //     <td>'+$(this).parents('.colone').find('input.label__poid').val()+'</td>\n\
            //     <td class="switch"><img  src="img/exchange.png"/></td>\n\
            //     <td class="poubelle"><img src="img/trash.png"/></td>\n\
            //   </tr>');
            $(this).parent('.form').parent('.colone').find('table tbody').append('\
              <tr id="id-'+id+'">\n\
                <td>'+$(this).parents('.colone').find('input.label__type').val()+'</td>\n\
                <td>'+$(this).parents('.colone').find('input.label__poid').val()+'</td>\n\
              </tr>');
            $('.poubelle').unbind('click');
            poubelle();
            $('.switch').unbind('click');
            switchES();
            $(this).parent('.form').find('input.label__type').val('');
            $(this).parent('.form').find('input.label__poid').val('');
        }else{
            //marche pas
            $(this).parent('.form').append('<span class="error">erreur de saisi</span>');
            window.setTimeout(function(){
                $(".error").remove();
            }, 2000);

        }
    });
});